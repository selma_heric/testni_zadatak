const config = require("./config.js"),
express      = require("express"),
mongoose     = require("mongoose"),
bodyParser   = require("body-parser"),
authController = require("./auth/auth.controller.js"),
app          = express();

const HOST_NAME = config.host_name,
PORT            = config.port,
DATABASE_NAME   = config.database_name;

//Database connection
mongoose.connect("mongodb://" + HOST_NAME + "/" + DATABASE_NAME, { useNewUrlParser: true })
  .then(() => console.log("Connected to MongoDB '" + DATABASE_NAME + "'."))
  .catch(err => console.log("Something went wrong", err))

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.get('/', (req, res) => {
    res.json({"message": "Welcome to my api :D"});
});
//auth
app.use('/auth', authController);
var bookRouter = require('./routes/book.routes.js')(app);
//var bookRouter = require('./routes/book.routes.js');
var authorRouter = require('./routes/author.routes.js')(app);
//app.use('/', bookRouter);

app.listen(PORT, "0.0.0.0", function(){
  console.log("SERVER IS RUNNING ON PORT " + PORT + ".");
});
