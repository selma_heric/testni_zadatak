const Author = require('../models/author.model.js');
const AuthorHelper = require('../helpers/author.helper.js');

////////////////////////////////////////////////////////////////////////////
exports.createNewAuthor = (req, res, next) => {
  Author.create(req.body, function(err, newAuthor){
    if(err){
      return res.status(500).json({
          message: err.message || "Some error occurred while creating new author."
      });
    } else{
       res.status(201).json(newAuthor);
    }
  });
}

////////////////////////////////////////////////////////////////////////////
exports.findAll = (req, res, next) => {
  Author.find({}, function(err, authors){
    if(err){
      return res.status(500).json({
          message: err.message || "Some error occurred while retrieving all authors."
      });
    } else{
      return res.status(200).json(authors);
    }
  });
}

////////////////////////////////////////////////////////////////////////////
exports.findAuthorById = (req, res, next) => {
  Author.findById(req.params.id, function(err, author){
    if(!author){
      return res.status(404).json({
        message: "Author does not exist."
      });
    }
    if(err){
      return res.status(500).json({
        message: err.message || "Some error occurred while retrieving the author."
      });
    }
    res.status(200).json(author);
  });
}

//////////////////////////////////////////////////////////////////////////
exports.updateAuthor = (req, res, next) => {
  Author.findByIdAndUpdate(req.params.id, req.body, {new: true}, function(err, author){
    if(!author){
      return res.status(404).json({
        message: "Author does not exist."
      });
    }
    if(err){
      return res.status(500).json({
        message: err.message || "Some error occurred while retrieving the author."
      });
    }
    res.status(200).json(author);
  });
}

//////////////////////////////////////////////////////////////////////////
exports.deleteAuthor = (req, res, next) => {
  Author.findByIdAndRemove(req.params.id, function(err, author){
    if(!author){
      return res.status(404).json({
        message: "Author does not exist."
      });
    }
    if(err){
      return res.status(500).json({
        message: err.message || "Some error occurred while deleting the author."
      });
    }
    res.status(200).json({
        message: "Author " + author.first_name + " " + author.last_name + " was deleted."
    });
  });
}

///////////////////////////////////////////////////////////////////////////
exports.getBooksOfAuthor = (req, res, next) => {
  Author.findById(req.params.id, function(err, author){
    if(!author){
      return res.status(404).json({
        message: "Author does not exist."
      });
    }
    if(err){
      return res.status(500).json({
        message: err.message || "Some error occurred while deleting the book."
      });
    }

    AuthorHelper.findBooksOfAuthor(req,res);

  });
}

////////////////////////////////////////////////////////////////////////////
exports.addNewBookToAuthor = (req, res, next) => {
  Author.findById(req.params.id, function(err, author){
    if(!author){
      return res.status(404).json({
        message: "Author does not exist."
      });
    }
    if(err){
      return res.status(500).json({
        message: err.message || "Some error occurred while deleting the book."
      });
    }
    AuthorHelper.addNewBookToAuthor(req, res, author);
  });
}

//////////////////////////////////////////////////////////////////////////////
exports.addExistingBookToAuthor = (req, res, next) => {
  Author.findById(req.params.idAuthor, function(err, author){
    if(!author){
      return res.status(404).json({
        message: "Author does not exist."
      });
    }
    if(err){
      return res.status(500).json({
        message: err.message || "Some error occurred while deleting the book."
      });
    }
    AuthorHelper.addExistingBookToAuthor(req, res, author);
  });
}

//////////////////////////////////////////////////////////////////////////////
exports.deleteBookFromAuthor = (req, res, next) => {
  Author.findById(req.params.idAuthor, function(err, author){
    if(!author){
      return res.status(404).json({
        message: "Author does not exist."
      });
    }
    if(err){
      return res.status(500).json({
        message: err.message || "Some error occurred while deleting the book."
      });
    }
    AuthorHelper.deleteBookFromAuthor(req, res, author);
  });
}
