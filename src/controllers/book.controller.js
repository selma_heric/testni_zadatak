const Book = require('../models/book.model.js');
const BookHelper = require('../helpers/book.helper.js');

/////////////////////////////////////////////////////////////////////////////
exports.createNewBook = (req, res, next) => {
  Book.create(req.body, function(err, newBook){
    if(err){
      return res.status(500).json({
          message: err.message || "Some error occurred while creating new book."
      });
    } else{
      return res.status(201).json(newBook);
    }
  });
}

/////////////////////////////////////////////////////////////////////////////
exports.findAll = (req, res, next) => {
  Book.find({}, function(err, books){
    if(err){
      return res.status(500).json({
          message: err.message || "Some error occurred while retrieving all books."
      });
    } else{
      return res.status(200).json(books);
    }
  }).populate("authors");
}

/////////////////////////////////////////////////////////////////////////////
exports.findBookById = (req, res, next) => {
  Book.findById(req.params.id, function(err, book){
    if(!book){
      return res.status(404).json({
        message: "Book does not exist."
      });
    }
    if(err){
      return res.status(500).json({
        message: err.message || "Some error occurred while retrieving the book."
      });
    }
    res.status(200).json(book);
  });
}

/////////////////////////////////////////////////////////////////////////////
exports.updateBook = (req, res, next) => {
  Book.findByIdAndUpdate(req.params.id, req.body, {new: true}, function(err, book){
    if(!book){
      return res.status(404).json({
        message: "Book does not exist."
      });
    }
    if(err){
      return res.status(500).json({
        message: err.message || "Some error occurred while retrieving one book."
      });
    }
    res.status(200).json(book);
  });
}

/////////////////////////////////////////////////////////////////////////////
exports.deleteBook = (req, res, next) => {
  Book.findByIdAndRemove(req.params.id, function(err, book){
    if(!book){
      return res.status(404).json({
        message: "Book does not exist."
      });
    }
    if(err){
      return res.status(500).json({
        message: err.message || "Some error occurred while deleting the book."
      });
    }
    res.status(200).json({
        message: "Book " + book.title + " was deleted."
    });
  });
}

////////////////////////////////////////////////////////////////////////////
exports.getAuthorsOfBook = (req, res, next) => {
  Book.findById(req.params.id, function(err, book){
    if(!book){
      return res.status(404).json({
        message: "Book does not exist"
      });
    }
    if(err){
      return res.status(500).json({
        message: err.message || "Some error occurred while deleting the book."
      });
    }
    res.json(book.authors);
  })
  .populate("authors");
}

/////////////////////////////////////////////////////////////////////////////
exports.addNewAuthorToBook = (req, res, next) => {
  Book.findById(req.params.id, function(err, book){
    if(!book){
      return res.status(404).json({
        message: "Book does not exist."
      });
    }
    if(err){
      return res.status(500).json({
        message: err.message || "Some error occurred while finding the book."
      });
    }
    //BookHelper.findAuthorByIdCodeAndAddItToBook(req.body.id_code, book, req, res);
    BookHelper.addNewBookToAuthor(req, res, book);
  });
}

//////////////////////////////////////////////////////////////////////////////
exports.addExistingAuthorToBook = (req, res, next) => {
  Book.findById(req.params.idBook, function(err, book){
    if(!book){
      return res.status(404).json({
        message: "Book does not exist."
      });
    }
    if(err){
      return res.status(500).json({
        message: err.message || "Some error occured while finding the book."
      });
    }
    BookHelper.addExistingAuthorToBook(req, res, book);
  });
}

////////////////////////////////////////////////////////////////////////////
exports.deleteAuthorFromBook = (req, res, next) => {
  Book.findById(req.params.idBook, function(err, book){
    if(!book){
      return res.status(404).json({
        message: "Book does not exist."
      })
    }
    if(err){
      return res.status(500).json({
        message: err.message || "Some error occurred while deleting the book."
      });
    }
    for(var i=0; i<book.authors.length; i++){
      if(book.authors[i].equals(req.params.idAuthor)){
        book.authors.splice(i,1);
        book.save();
        return res.status(200).json(book);
      }
    }
    return res.status(404).json({
      message: "Author with this id is not connected to this book."
    })
  });
}
