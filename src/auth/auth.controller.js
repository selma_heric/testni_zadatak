const express = require("express");
const router = express.Router();

var bodyParser = require("body-parser");

router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());

var jwt = require("jsonwebtoken");
var bcrypt = require("bcryptjs");
var config = require("../config");

var user_db = "test";
var user_id = 1
var password_db = "pass123";
var hashedPassword = bcrypt.hashSync(password_db, 8);

router.post("/login", function(req, res){
  if(user_db === req.body.username){
    var passwordIsValid = bcrypt.compareSync(req.body.password, hashedPassword);
    if(!passwordIsValid){
      return res.status(401).json({
        auth: false,
        token: null
      });
    }
    var token = jwt.sign({id: user_id}, config.secret, { expiresIn: 86400 }); //24 hours
    return res.status(200).json({
      auth: true,
      token: token
    });

  }
  return res.status(404).json({
    message: "Invalid username."
  })
});

var verifyToken = require("./verify.token.js");

//TESTNA RUTA
router.get("/me", verifyToken, function(req, res, next){
  if(user_db != req.body.username){
    return res.status(404).json({
      message: "Invalid username"
    });
  }
  var user = {
    id: user_id,
    username: user_db,
    password: hashedPassword
  }
  return res.status(200).json(user);
});

router.use(function (user, req, res, next) {
  res.status(200).json(user);
});

//POKUSALA DA UNISTIM TOKEN, ALI IZGLEDA DA SE TO NE MOZE URADITI SA JWT
/*router.get('/logout', function(req, res) {
  res.status(200).send({ auth: false, token: null });
});*/

module.exports = router;
