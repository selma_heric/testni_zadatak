
module.exports = (app) => {
    const books = require('../controllers/book.controller.js');
    var verifyToken = require("../auth/verify.token.js");

    //Create new book
    app.post("/books",verifyToken, books.createNewBook);

    //Retrieve all books
    app.get("/books",verifyToken, books.findAll);

    //Retrieve book by id
    app.get("/books/:id",verifyToken, books.findBookById);

    //Update book
    app.put("/books/:id",verifyToken, books.updateBook);

    //Delete book
    app.delete("/books/:id",verifyToken, books.deleteBook);

    //Get list of authors for book
    app.get("/books/:id/authors",verifyToken, books.getAuthorsOfBook);

    //Add new author to {id} book
    app.post("/books/:id/authors",verifyToken, books.addNewAuthorToBook);

    //Add already exsisting {id} author from db to {id} book
    app.post("/books/:idBook/authors/:idAuthor",verifyToken, books.addExistingAuthorToBook);

    //Delete {id} author from {id} book
    app.delete("/books/:idBook/authors/:idAuthor",verifyToken, books.deleteAuthorFromBook);
  }
