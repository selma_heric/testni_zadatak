module.exports = (app) => {
    const authors = require('../controllers/author.controller.js');
    var verifyToken = require("../auth/verify.token.js");

    //Create new author
    app.post("/authors",verifyToken, authors.createNewAuthor);

    //Retrieve all authors
    app.get("/authors",verifyToken, authors.findAll);

    //Retrieve author by id
    app.get("/authors/:id",verifyToken, authors.findAuthorById);

    //Update author
    app.put("/authors/:id",verifyToken, authors.updateAuthor);

    //Delete author
    app.delete("/authors/:id",verifyToken, authors.deleteAuthor);

    //Get list of books for author
    app.get("/authors/:id/books",verifyToken, authors.getBooksOfAuthor);

    //Add new book to {id} author
    app.post("/authors/:id/books",verifyToken, authors.addNewBookToAuthor);

    //Add existing {id} book to {id} author
    app.post("/authors/:idAuthor/books/:idBook",verifyToken, authors.addExistingBookToAuthor);

    //Delete {id} book from {id} authors
    app.delete("/authors/:idAuthor/books/:idBook",verifyToken, authors.deleteBookFromAuthor);
  }
