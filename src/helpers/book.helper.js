const Author = require('../models/author.model.js');

//OVO SAM ZAKOMPLIKOVALA BEZVEZE ALI HAJDE RADI
//JEDNOSTAVNIJA FUNKCIJA JE ISPOD
exports.findAuthorByIdCodeAndAddItToBook = (id_code, book, req, res) => {
  Author.find({id_code: id_code}, function(err, author){
    if(author.length == 0){
      //console.log("Author not found");
      Author.create(req.body, function(err, newAuthor){
        if(err){
          return res.status(500).json({
            //sucess: "false",
              message: err.message || "Some error occurred while creating new author."
          });
        } else {
           book.authors.push(newAuthor._id);
           book.save();
           return res.status(200).json(book);
        }
      });
    } else {
      for(var i=0; i<book.authors.length; i++){
        //console.log(book.authors[i]);
        //console.log(author[0]._id);
        if(book.authors[i].equals(author[0]._id)){
          return res.json({
            message: "Author with this id_code is already added to this book."
          });
        }
      }
      book.authors.push(author._id);
      book.save();
      return res.json(book);
    }
  });
}

////////////////////////////////////////////////////////////////////////////
exports.addNewBookToAuthor = (req, res, book) => {
  Author.create(req.body, function(err, newAuthor){
    if(err){
      return res.status(500).json({
          message: err.message || "Some error occurred while creating new author."
      });
    } else{
      book.authors.push(newAuthor._id);
      book.save()
      return res.status(200).json(book);
    }
  });
}

////////////////////////////////////////////////////////////////////////////
exports.addExistingAuthorToBook = (req, res, book) => {
  Author.findById(req.params.idAuthor, function(err, author){
    if(!author){
      return res.status(404).json({
        message: "Author with this id does not exist in database."
      })
    }
    if(err){
      return res.status(500).json({
        message: err.message || "Some error occured while retreiving author by id."
      });
    }
    for(var i=0; i<book.authors.length; i++){
      //console.log(book.authors[i]);
      //console.log(author._id);
      if(book.authors[i].equals(author._id)){
        return res.json({
          message: "Author with this id is already added to this book."
        });
      }
    }
    book.authors.push(author._id);
    book.save();
    return res.json(book);
  });
}
