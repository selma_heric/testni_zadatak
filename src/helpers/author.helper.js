const Book = require('../models/book.model.js');

////////////////////////////////////////////////////////////////////////////
exports.findBooksOfAuthor = (req, res) => {
  Book.find({authors: req.params.id}, function(err, books){
    if(err){
      return res.status(500).json({
        message: err.message || "Some error occurred while retreiving books."
      })
    } else {
      return res.status(200).json(books);
    }
  });
}

////////////////////////////////////////////////////////////////////////////
exports.addNewBookToAuthor = (req, res, author) => {
  Book.create(req.body, function(err, newBook){
    if(err){
      return res.status(500).json({
          message: err.message || "Some error occurred while creating new book."
      });
    } else{
      newBook.authors.push(req.params.id);
      newBook.save();
      return res.status(201).json(newBook);
    }
  });
}

/////////////////////////////////////////////////////////////////////////////
exports.addExistingBookToAuthor = (req, res, author) => {
  Book.findById(req.params.idBook, function(err, book){
    if(!book){
      return res.status(404).json({
        message: "Book with this id does not exist in database."
      })
    }
    if(err){
      return res.status(500).json({
        message: err.message || "Some error occured while retreiving author by id."
      });
    }
    for(var i=0; i<book.authors.length; i++){
      if(book.authors[i].equals(author._id)){
        return res.status(200).json({
          message: "Author with this id is already added to this book."
        })
      }
    }
    book.authors.push(author._id);
    book.save();
    return res.status(200).json(book);
  });
}

////////////////////////////////////////////////////////////////////////////
exports.deleteBookFromAuthor = (req, res, author) => {
  Book.findById(req.params.idBook, function(err, book){
    if(!book){
      return res.status(404).json({
        message: "Book with this id does not exist in database."
      })
    }
    if(err){
      return res.status(500).json({
        message: err.message || "Some error occured while retreiving author by id."
      });
    }
    for(var i=0; i<book.authors.length; i++){
      if(book.authors[i].equals(author._id)){
        book.authors.splice(i,1);
        book.save();
        return res.status(200).json(book);
      }
    }
    return res.status(404).json({
      message: "Book with this id is not connected with this author."
    })
  });
}
