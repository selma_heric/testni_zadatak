const mongoose = require("mongoose");
const Schema = mongoose.Schema;

var Author = require("./author.model.js");

const bookSchema = new Schema({
  title: { type: String, required: true },
  number_of_pages: { type: Number, required: true },
  id_code: {type: Number, required: true, unique: true },
  authors:  [{ type: Schema.Types.ObjectId, ref: "Author" }]
});

/*const bookSchema = new Schema({
  title: { type: String, required: true },
  number_of_pages: { type: Number, required: true }
});*/

module.exports = mongoose.model("Book", bookSchema);
