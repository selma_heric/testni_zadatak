const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const authorSchema = new Schema({
  first_name: { type: String, required: true },
  last_name: { type: String, required: true },
  middle_name: {type: String },
  id_code: { type: Number, requred: true, unique: true }
});

module.exports = mongoose.model("Author", authorSchema);
