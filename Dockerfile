FROM node:10.15.0

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY . /usr/src/app
COPY package.json /usr/src/app/

VOLUME [ "/usr/src/app" ]

EXPOSE 3000
RUN npm install
CMD ["node", "src/app.js"]
