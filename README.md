# docker-express-mongodb

> Docker with Express and MongoDB.

# Development

* Installing dependencies

```bash
$ npm install
```

* Running scripts

```bash
$ node app.js
```
> app.js is in foler src

# Docker

> Before using docker in file config.js set host_name : "mongodb"

* Building an image

```bash
$ docker-compose build
```

* Running a container

```bash
$ docker-compose up
```

* Stopping a container

```bash
$ docker-compose down
```

# Postman

* https://documenter.getpostman.com/view/6279926/RznEJdL1
